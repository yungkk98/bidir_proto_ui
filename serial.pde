import controlP5.*;

ControlP5 cp5;
bluetooth b;

grip left, right;
Boolean[][][] vib = new Boolean[2][3][3];

void setup(){
  fullScreen(P3D);
  
  /*
  b = new bluetooth(this);
  b.connect();
  */

  left = new grip(-1);
  right = new grip(1);
  
  for(int k=0; k<2; k++){
    for(int i=0; i<3; i++){
      for(int j=0; j<3; j++){
        vib[k][i][j] = false;
      }
    }
  }
  vib[0][1][1] = true;
  
  setCP5();
}

void draw(){
  background(80);
  noStroke();
  fill(255);
  
  drawHandle();
  drawVibMat();
  drawGrip();
}

void drawHandle(){
  noStroke();
  fill(120);
  
  pushMatrix();
  translate(550, 600);
  rectMode(CENTER);
  rect(0, 0, 900, 100, 10);
  rect(0, 190, 120, 430, 10);
  popMatrix();
}

void drawVibMat(){
  noStroke();
  fill(125, 50, 50);
  
  pushMatrix();
  translate(550, 600);
  rectMode(CENTER);
  rect(-250, 0, 300, 40, 5);
  rect(250, 0, 300, 40, 5);
  
  rect(-250, 40, 300, 30, 5);
  rect(250, 40, 300, 30, 5);
  
  rect(-250, -40, 300, 30, 5);
  rect(250, -40, 300, 30, 5);
  
  for(int k=0; k<2; k++){
    for(int i=0; i<3; i++){
      for(int j=0; j<3; j++){
        if(vib[k][i][j]){
          fill(100, 100, 200);
        }
        else{
          fill(20, 20, 20);
        }
        rect(-350 + 100 * j + 500 * k, -40 + 40 * i, 20, 15);
      }
    }
  }
  popMatrix();
}

void drawGrip(){
  pushMatrix();
  translate(1300, 600);
  left.draw();
  popMatrix();
  pushMatrix();
  translate(1600, 600);
  right.draw();
  popMatrix();
}
