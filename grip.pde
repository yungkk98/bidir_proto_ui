class grip{
  private float angle;
  private int dir;
  
  public grip(int _dir){
    dir = _dir;
    angle = 0;
  }
  
  public void setAngle(float _angle){
    angle = _angle;
  }
  
  void draw(){
    fill(120);
    noStroke();
    ellipse(0, 0, 150, 150);
    
    translate(80 * dir, 0);
    rotateZ(angle * dir);
    translate(-80 * dir, 0);
    
    stroke(125, 50, 50);
    strokeWeight(8);
    line(-80, 0, -60, -80);
    line(-60, -80, 60, -80);
    line(60, -80, 80, 0);
  }
  
}
