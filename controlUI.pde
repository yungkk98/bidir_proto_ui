void setCP5(){
  cp5 = new ControlP5(this);
  ControlFont cf1 = new ControlFont(createFont("Arial",12));
  
  cp5.addButton("pattern_A")
     .setValue(0)
     .setPosition(150,300)
     .setSize(150,100)
     .setFont(cf1)
     ;
  
  // and add another 2 buttons
  cp5.addButton("pattern_B")
     .setValue(100)
     .setPosition(370,300)
     .setSize(150,100)
     .setFont(cf1)
     ;
     
  cp5.addButton("pattern_C")
     .setPosition(580,300)
     .setSize(150,100)
     .setValue(0)
     .setFont(cf1)
     ;
     
  cp5.addButton("pattern_D")
     .setPosition(800,300)
     .setSize(150,100)
     .setValue(0)
     .setFont(cf1)
     ;
     
  cp5.addSlider("slider_left")
     .setPosition(1200,300)
     .setSize(500,30)
     .setRange(0,180)
     .setValue(0)
     .setFont(cf1)
     ;
  // reposition the Label for controller 'slider'
  cp5.getController("slider_left").getValueLabel().align(ControlP5.LEFT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
  cp5.getController("slider_left").getCaptionLabel().align(ControlP5.RIGHT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);

  cp5.addSlider("slider_right")
     .setPosition(1200,360)
     .setSize(500,30)
     .setRange(0,180)
     .setValue(0)
     .setFont(cf1)
     ;
  // reposition the Label for controller 'slider'
  cp5.getController("slider_right").getValueLabel().align(ControlP5.LEFT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
  cp5.getController("slider_right").getCaptionLabel().align(ControlP5.RIGHT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
}

void slider_left(float angle){
  /* angle 0 ~ 180 (deg) */
  println("left: " + angle);
  float rad = radians(angle);
  left.setAngle(rad);
  if(b != null && b.isConnected()){
    /* send protocol */
    // b.write("---\r\n");
  }
  return;
}

void slider_right(float angle){
  /* angle 0 ~ 180 (deg) */
  println("right: " + angle);
  float rad = radians(angle);
  right.setAngle(rad);
  if(b != null && b.isConnected()){
    /* send protocol */
  }
  return;
}

public void pattern_A(int theValue){
  /* pattern A */
  println("pattern A: " + theValue);
  return;
}

public void pattern_B(int theValue){
  /* pattern B */
  println("pattern B: " + theValue);
  return;
}

public void pattern_C(int theValue){
  /* pattern C */
  println("pattern C: " + theValue);
  return;
}

public void pattern_D(int theValue){
  /* pattern D */
  println("pattern D: " + theValue);
  return;
}
