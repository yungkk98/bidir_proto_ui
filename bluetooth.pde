import processing.serial.*;

class bluetooth{
  Serial port;
  private boolean connected = false;
  
  public bluetooth(PApplet parent){
    printArray(Serial.list());
    port = new Serial(parent, Serial.list()[0], 57600);
  }
  
  public Boolean isConnected(){
    isDisconnected();
    return connected;
  }
  
  public void isDisconnected(){
    String inBuffer = port.readStringUntil('\n');
    if(inBuffer != null){
      println(inBuffer);
      if("+Disconnected\r\n".equals(inBuffer)){
        exit();
      }
    }
  }
  
  public void send(String s){
    if(isConnected() && s != null){
      port.write(s + "\r\n");
    }
    return;
  }
  
  public Boolean connect(){
    port.write("AT\r\n");
    println("AT");
    while(true){
      String inBuffer = port.readStringUntil('\n');
      if(inBuffer != null){
        println(inBuffer);
        break;
      }
    }
    
    port.write("AT+INQ\r\n");
    println("AT+INQ");
    while(true){
      String inBuffer = port.readStringUntil('\n');
      if(inBuffer != null){
        println(inBuffer);
        if("+INQE\r\n".equals(inBuffer)){
          break;
        }
      }
    }
    
    while(true){
      String inBuffer = port.readStringUntil('\n');
      if(inBuffer != null){
        println(inBuffer);
        int dev_cnt = int(inBuffer.split(" ")[2]);
        if(dev_cnt == 0){
          return false;
        }
        else{
          break;
        }
      }
    }
    
    port.write("AT+CONN1\r\n");
    println("AT+CONN1");
    while(true){
      String inBuffer = port.readStringUntil('\n');
      if(inBuffer != null){
        println(inBuffer);
        if("+Connected\r\n".equals(inBuffer)){
          connected = true;
          return true;
        }
        else{
          return false;
        }
      }
    }
    
  }
}
